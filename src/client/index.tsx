import * as React from 'react';
import * as ReactDOM from 'react-dom';
import RedBox from 'redbox-react';
import { Routes } from './routes/index';
// Store initialization

// DOM Element for render
const MOUNT_NODE = document.getElementById('root');

let render = () => {
  ReactDOM.render(<Routes />, MOUNT_NODE);
};

// ================================
// Development error view
// ================================
if (__DEV__) {
  if ((module as any).hot) {
    const renderApp = render;
    const renderError = (error: any) => {
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    render = () => {
      try {
        renderApp();
      } catch (e) {
        console.error(e);
        renderError(e);
      }
    };

    // Setup hot module replacement
    (module as any).hot.accept(['./containers/Root', './routes/index'], () =>
      setImmediate(() => {
        if (MOUNT_NODE) {
          ReactDOM.unmountComponentAtNode(MOUNT_NODE);
          render();
        }
      })
    );
  }
}

// Let's Go!
// ------------------------------------
if (!__TEST__) render();
