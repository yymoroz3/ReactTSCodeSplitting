import * as React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

import Home from './Home/loadable';
import Home2 from './Home2/loadable';

export const Routes: React.SFC<any> = () => {
  return (
    <BrowserRouter>
      <div style={{ position: 'relative', width: '100%', height: '100%' }}>
        <div className="container text-center">
          <h1>React Redux Starter Kit</h1>
          <NavLink to="/">Home1</NavLink>
          <NavLink to="/test">Home2</NavLink>
          <div className="page-layout__viewport">
            <Route path="/" component={Home} />
            <Route path="/test" component={Home2} />
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
};
