import * as React from 'react';
import { Loadable as ILoadable } from 'react-loadable';
const Loadable: ILoadable = require('react-loadable');

const Loader = () => {
  return <h1>Load...</h1>;
};

export default Loadable({
  loader: () => import('./HomeView2'),
  loading: Loader,
});
