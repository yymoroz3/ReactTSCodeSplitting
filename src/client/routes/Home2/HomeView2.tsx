import * as React from 'react';
import Test from './Test';

class Home2 extends React.Component<{}, {}> {
  public render() {
    return (
      <div>
        <h1>Hello2!</h1>
        <Test />
      </div>
    );
  }
}

export default Home2;
