import * as React from 'react';

class Home extends React.Component<{}, {}> {
  public render() {
    return <h1>Hello!</h1>;
  }
}

export default Home;
