module.exports = {
  parser: 'postcss-scss',
  sourceMap: true,
  plugins: {
    precss: {},
    cssnano: {
      preset: [
        'default',
        {
          discardComments: {
            removeAll: true
          }
        }
      ]
    },
    autoprefixer: {}
  }
};
