const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

//===========================
// ENV
//===========================
const env = process.env.NODE_ENV || 'development';

const __DEV__ = env === 'development';
const __TEST__ = env === 'test';
const __PROD__ = env === 'production';

//===========================
// Config
//===========================
const config = {
  devtool: __DEV__ ? 'eval-source-map' : 'cheap-module-source-map',

  entry: {
    //resources: ['./src/client/resources.tsx'],
    index: ['./src/client/index.tsx'],
  },
  output: {
    filename: '[name].[chunkhash].js',
    path: path.join(__dirname, '../public'),
    chunkFilename: '[name].[chunkhash].chunk.js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js', '.json'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        // loader: 'ts-loader',
        loader: ['awesome-typescript-loader'],
        exclude: [/node_modules/],
      },
    ],
  },

  plugins: [
    /*  new webpack.LoaderOptionsPlugin({
      minimize: true,
    }), */
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(env) },
      __DEV__,
      __TEST__,
      __PROD__,
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/client/index.html',
      minify: {
        removeAttributeQuotes: __PROD__,
        collapseWhitespace: __PROD__,
        html5: __PROD__,
        removeComments: __PROD__,
        removeEmptyAttributes: __PROD__,
      },
    }),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      sourceMap: false,
      compress: {
        screw_ie8: true,
        warnings: false,
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'libs',
      minChunks: module => {
        return module.context && module.context.indexOf('node_modules') !== -1; //&& count > 1;
      },
      filename: '[name].js', // Specify the common bundle's name.
    }),
  ],
};

//===========================
// Set DEV config
//===========================
if (__DEV__) {
  //config.entry.entry.push(`webpack-hot-middleware/client.js?path=${config.output.publicPath}__webpack_hmr`);
  config.plugins.push(new webpack.HotModuleReplacementPlugin(), new webpack.NamedModulesPlugin());
}

module.exports = config;
